package com.example;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void sortTest()
    {
        Integer[] list = {5,4,3,2,1,-5,-2};
        Integer[] res = {-5,-2,1,2,3,4,5};
        Assert.assertArrayEquals(res, App.sort(list));
        
    }
}
